import { TestBed } from '@angular/core/testing';

import { ApicommunicationService } from './apicommunication.service';

describe('ApicommunicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ApicommunicationService = TestBed.get(ApicommunicationService);
    expect(service).toBeTruthy();
  });
});
