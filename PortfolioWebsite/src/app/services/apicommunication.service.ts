import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { retry, catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApicommunicationService {
  apiUrl = "https://api.github.com";

  constructor(private http: HttpClient) { }

  public sendPost(requestPath: String, content: Array<any>) {

    // Post body
    const requestContent = new URLSearchParams();

    // Post content
    for (let i = 0; i < content.length; i++) {
      requestContent.set(content[i][0], content[i][1]);
    }

    // Setting request type
    const requestType = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    // Returning Post

    
    return this.http.post(this.apiUrl + requestPath, requestContent.toString(), requestType).pipe(
     catchError(this.handleError)
    );

  }

  public sendGet(requestPath: String) {

    // Setting request type
    const requestType = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };

    return this.http.get(this.apiUrl + requestPath, requestType).pipe(
      catchError(this.handleError)
    )
  }

  // Handling JSON parse error
  handleError(error) {
    setTimeout( () => {
      window.alert('Die Daten konnten nicht geladen werden. Bitte versuchen Sie es später erneut.');
    });
    return throwError(error);
  }
  } 


