import { ChangeDetectorRef, Component } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public mobileQuery: MediaQueryList;
  public _mobileQueryListener: () => void;
  title = 'PortfolioWebsite';

  constructor(
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher
  ){
    this.mobileQuery = media.matchMedia('(max-width: 1024px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }
}
